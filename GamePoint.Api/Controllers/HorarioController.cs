using Microsoft.AspNetCore.Mvc;
using GamePoint.Entity;
using GamePoint.Service;

namespace GamePoint.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HorarioController: ControllerBase
    {
        private IHorarioService horarioService;
        public HorarioController(IHorarioService horarioService){
            this.horarioService = horarioService;
        }


        [HttpGet]
        public ActionResult Get(){
            return Ok(horarioService.Listar());
        }
        [HttpPost]
        public ActionResult Post([FromBody] Horario horario){
            return Ok(horarioService.Guardar(horario));
        }
        [HttpPut]
        public ActionResult Put([FromBody] Horario horario){
            return Ok(horarioService.Actualizar(horario));
        }
        [HttpDelete("{id}")]
        public ActionResult Delete(int id){
            return Ok(horarioService.Eliminar(id));
        }
        [HttpGet("id")]
        public ActionResult Get(int id){
            return Ok(horarioService.ListarPorId(id));
        }


        [HttpGet("DisponibilidadCabina/{idC}/{idL}")]
        public ActionResult GetCabinaHorario(int idC, int idL){
            return Ok(horarioService.GetCabinaHorarioPorLan(idC, idL));
        }
    }
}