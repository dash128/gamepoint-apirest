using Microsoft.AspNetCore.Mvc;
using GamePoint.Entity;
using GamePoint.Service;

namespace GamePoint.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RetoController: ControllerBase
    {
        private IRetoService retoService;
        public RetoController(IRetoService retoService){
            this.retoService = retoService;
        }


        [HttpGet]
        public ActionResult Get(){
            return Ok(retoService.Listar());
        }
        [HttpPost]
        public ActionResult Post([FromBody] Reto reto){
            return Ok(retoService.Guardar(reto));
        }
        [HttpPut]
        public ActionResult Put([FromBody] Reto reto){
            return Ok(retoService.Actualizar(reto));
        }
        [HttpDelete("{id}")]
        public ActionResult Delete(int id){
            return Ok(retoService.Eliminar(id));
        }
        [HttpGet("id")]
        public ActionResult Get(int id){
            return Ok(retoService.ListarPorId(id));
        }



    }
}