using Microsoft.AspNetCore.Mvc;
using GamePoint.Entity;
using GamePoint.Service;

namespace GamePoint.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class JuegoPorLanCenterController: ControllerBase
    {
        private IJuegoPorLanCenterService juegoPorLanCenterService;
        public JuegoPorLanCenterController(IJuegoPorLanCenterService juegoPorLanCenterService){
            this.juegoPorLanCenterService = juegoPorLanCenterService;
        }


        [HttpGet]
        public ActionResult Get(){
            return Ok(juegoPorLanCenterService.Listar());
        }
        [HttpPost]
        public ActionResult Post([FromBody] JuegoPorLanCenter juegoPorLanCenter){
            return Ok(juegoPorLanCenterService.Guardar(juegoPorLanCenter));
        }
        [HttpPut]
        public ActionResult Put([FromBody] JuegoPorLanCenter juegoPorLanCenter){
            return Ok(juegoPorLanCenterService.Actualizar(juegoPorLanCenter));
        }
        [HttpDelete("{id}")]
        public ActionResult Delete(int id){
            return Ok(juegoPorLanCenterService.Eliminar(id));
        }
        [HttpGet("id")]
        public ActionResult Get(int id){
            return Ok(juegoPorLanCenterService.ListarPorId(id));
        }


        [HttpGet("BuscarJuegoPorLanCenter/{id}")]
        public ActionResult GetJuegos(int id){
            return Ok(juegoPorLanCenterService.ListarJuegoPorLanCenter(id));
        }

        [HttpGet("BuscarLanCenterPorJuego/{id}")]
        public ActionResult GetLanCenters(int id){
            return Ok(juegoPorLanCenterService.ListarLanCenterPorJuego(id));
        }

        
    }
}