using Microsoft.AspNetCore.Mvc;
using GamePoint.Entity;
using GamePoint.Service;

namespace GamePoint.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class JugadorController: ControllerBase
    {
        private IJugadorService jugadorService;
        public JugadorController(IJugadorService jugadorService){
            this.jugadorService = jugadorService;
        }


        [HttpGet]
        public ActionResult Get(){
            return Ok(jugadorService.Listar());
        }
        [HttpPost]
        public ActionResult Post([FromBody] Jugador jugador){
            return Ok(jugadorService.Guardar(jugador));
        }
        [HttpPut]
        public ActionResult Put([FromBody] Jugador jugador){
            return Ok(jugadorService.Actualizar(jugador));
        }
        [HttpDelete("{id}")]
        public ActionResult Delete(int id){
            return Ok(jugadorService.Eliminar(id));
        }
        [HttpGet("id")]
        public ActionResult Get(int id){
            return Ok(jugadorService.ListarPorId(id));
        }


        [HttpPost("validate")]
        public ActionResult ValidarCuenta([FromBody] Jugador jugador){
            return Ok(jugadorService.ValidarCorreo(jugador.Correo, jugador.Contrasena));
        }
    }
}