using Microsoft.AspNetCore.Mvc;
using GamePoint.Entity;
using GamePoint.Service;

namespace GamePoint.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LanCenterController: ControllerBase
    {
        private ILanCenterService lancenterService;
        public LanCenterController(ILanCenterService lancenterService){
            this.lancenterService = lancenterService;
        }


        [HttpGet]
        public ActionResult Get(){
            return Ok(lancenterService.Listar());
        }
        [HttpPost]
        public ActionResult Post([FromBody] LanCenter lanCenter){
            return Ok(lancenterService.Guardar(lanCenter));
        }
        [HttpPut]
        public ActionResult Put([FromBody] LanCenter lanCenter){
            return Ok(lancenterService.Actualizar(lanCenter));
        }
        [HttpDelete("{id}")]
        public ActionResult Delete(int id){
            return Ok(lancenterService.Eliminar(id));
        }
        [HttpGet("id")]
        public ActionResult Get(int id){
            return Ok(lancenterService.ListarPorId(id));
        }


        [HttpGet("BuscarConNombre/{nombre}")]
        public ActionResult GetConNombre(string nombre){
            return Ok(lancenterService.ListarConNombre(nombre));
        }
        [HttpGet("BuscarConDistrito/{distrito}")]
        public ActionResult GetConDistrito(string distrito){
            return Ok(lancenterService.ListarConDistrito(distrito));
        }

    }
}