using Microsoft.AspNetCore.Mvc;
using GamePoint.Entity;
using GamePoint.Service;

namespace GamePoint.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SuscripcionController: ControllerBase
    {
        private ISuscripcionService suscripcionService;
        public SuscripcionController(ISuscripcionService suscripcionService){
            this.suscripcionService = suscripcionService;
        }


        [HttpGet]
        public ActionResult Get(){
            return Ok(suscripcionService.Listar());
        }
        [HttpPost]
        public ActionResult Post([FromBody] Suscripcion suscripcion){
            return Ok(suscripcionService.Guardar(suscripcion));
        }
        [HttpPut]
        public ActionResult Put([FromBody] Suscripcion suscripcion){
            return Ok(suscripcionService.Actualizar(suscripcion));
        }
        [HttpDelete("{id}")]
        public ActionResult Delete(int id){
            return Ok(suscripcionService.Eliminar(id));
        }
        [HttpGet("id")]
        public ActionResult Get(int id){
            return Ok(suscripcionService.ListarPorId(id));
        }
    }
}