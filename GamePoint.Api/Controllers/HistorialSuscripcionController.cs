using Microsoft.AspNetCore.Mvc;
using GamePoint.Entity;
using GamePoint.Service;

namespace GamePoint.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HistorialSuscripcionController: ControllerBase
    {
        private IHistorialSuscripcionService historialSuscripcionService;
        public HistorialSuscripcionController(IHistorialSuscripcionService historialSuscripcionService){
            this.historialSuscripcionService = historialSuscripcionService;
        }


        [HttpGet]
        public ActionResult Get(){
            return Ok(historialSuscripcionService.Listar());
        }
        [HttpPost]
        public ActionResult Post([FromBody] HistorialSuscripcion historialSuscripcion){
            return Ok(historialSuscripcionService.Guardar(historialSuscripcion));
        }
        [HttpPut]
        public ActionResult Put([FromBody] HistorialSuscripcion historialSuscripcion){
            return Ok(historialSuscripcionService.Actualizar(historialSuscripcion));
        }
        [HttpDelete("{id}")]
        public ActionResult Delete(int id){
            return Ok(historialSuscripcionService.Eliminar(id));
        }
        [HttpGet("id")]
        public ActionResult Get(int id){
            return Ok(historialSuscripcionService.ListarPorId(id));
        }
    }
}