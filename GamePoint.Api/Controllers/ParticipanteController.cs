using Microsoft.AspNetCore.Mvc;
using GamePoint.Entity;
using GamePoint.Service;

namespace GamePoint.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParticipanteController: ControllerBase
    {
        private IParticipanteService participanteService;
        public ParticipanteController(IParticipanteService participanteService){
            this.participanteService = participanteService;
        }


        [HttpGet]
        public ActionResult Get(){
            return Ok(participanteService.Listar());
        }
        [HttpPost]
        public ActionResult Post([FromBody] Participante participante){
            return Ok(participanteService.Guardar(participante));
        }
        [HttpPut]
        public ActionResult Put([FromBody] Participante participante){
            return Ok(participanteService.Actualizar(participante));
        }
        [HttpDelete("{id}")]
        public ActionResult Delete(int id){
            return Ok(participanteService.Eliminar(id));
        }
        [HttpGet("id")]
        public ActionResult Get(int id){
            return Ok(participanteService.ListarPorId(id));
        }
    }
}