using Microsoft.AspNetCore.Mvc;
using GamePoint.Entity;
using GamePoint.Service;

namespace GamePoint.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AmigoController: ControllerBase
    {
        private IAmigoService amigoService;
        public AmigoController(IAmigoService amigoService){
            this.amigoService = amigoService;
        }
        

        [HttpGet]
        public ActionResult Get(){
            return Ok(amigoService.Listar());
        }
        [HttpPost]
        public ActionResult Post([FromBody] Amigo amigo){
            return Ok(amigoService.Guardar(amigo));
        }
        [HttpPut]
        public ActionResult Put([FromBody] Amigo amigo){
            return Ok(amigoService.Actualizar(amigo));
        }
        [HttpDelete("{id}")]
        public ActionResult Delete(int id){
            return Ok(amigoService.Eliminar(id));
        }
        [HttpGet("id")]
        public ActionResult Get(int id){
            return Ok(amigoService.ListarPorId(id));
        }

    }
}