using Microsoft.AspNetCore.Mvc;
using GamePoint.Entity;
using GamePoint.Service;

namespace GamePoint.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CabinaController: ControllerBase
    {
        private ICabinaService cabinaService;
        public CabinaController(ICabinaService cabinaService){
            this.cabinaService = cabinaService;
        }


        [HttpGet]
        public ActionResult Get(){
            return Ok(cabinaService.Listar());
        }
        [HttpPost]
        public ActionResult Post([FromBody] Cabina cabina){
            return Ok(cabinaService.Guardar(cabina));
        }
        [HttpPut]
        public ActionResult Put([FromBody] Cabina cabina){
            return Ok(cabinaService.Actualizar(cabina));
        }
        [HttpDelete("{id}")]
        public ActionResult Delete(int id){
            return Ok(cabinaService.Eliminar(id));
        }
        [HttpGet("id")]
        public ActionResult Get(int id){
            return Ok(cabinaService.ListarPorId(id));
        }

        [HttpGet("ListarPorLanCenter/id")]
        public ActionResult GetPorLanCenter(int id){
            return Ok(cabinaService.ListarCabinaPorLanCenter(id));
        }
        
    }
}