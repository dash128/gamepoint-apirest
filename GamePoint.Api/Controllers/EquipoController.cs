using Microsoft.AspNetCore.Mvc;
using GamePoint.Entity;
using GamePoint.Service;

namespace GamePoint.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EquipoController:ControllerBase
    {
        private IEquipoService equipoService;
        public EquipoController(IEquipoService equipoService){
            this.equipoService = equipoService;
        }


        [HttpGet]
        public ActionResult Get(){
            return Ok(equipoService.Listar());
        }
        [HttpPost]
        public ActionResult Post([FromBody] Equipo equipo){
            return Ok(equipoService.Guardar(equipo));
        }
        [HttpPut]
        public ActionResult Put([FromBody] Equipo equipo){
            return Ok(equipoService.Actualizar(equipo));
        }
        [HttpDelete("{id}")]
        public ActionResult Delete(int id){
            return Ok(equipoService.Eliminar(id));
        }
        [HttpGet("id")]
        public ActionResult Get(int id){
            return Ok(equipoService.ListarPorId(id));
        }
    }
}