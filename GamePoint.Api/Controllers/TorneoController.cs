using Microsoft.AspNetCore.Mvc;
using GamePoint.Entity;
using GamePoint.Service;

namespace GamePoint.Api.Controllers
{
    public class TorneoController: ControllerBase
    {
        private ITorneoService torneoService;
        public TorneoController(ITorneoService torneoService){
            this.torneoService = torneoService;
        }

        [HttpGet]
        public ActionResult Get(){
            return Ok(torneoService.Listar());
        }
        [HttpPost]
        public ActionResult Post([FromBody] Torneo torneo){
            return Ok(torneoService.Guardar(torneo));
        }
        [HttpPut]
        public ActionResult Put([FromBody] Torneo torneo){
            return Ok(torneoService.Actualizar(torneo));
        }
        [HttpDelete("{id}")]
        public ActionResult Delete(int id){
            return Ok(torneoService.Eliminar(id));
        }
        [HttpGet("id")]
        public ActionResult Get(int id){
            return Ok(torneoService.ListarPorId(id));
        }
    }
}