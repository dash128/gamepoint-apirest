using Microsoft.AspNetCore.Mvc;
using GamePoint.Entity;
using GamePoint.Service;

namespace GamePoint.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReservaController: ControllerBase
    {
        private IReservaService reservaService;
        public ReservaController(IReservaService reservaService){
            this.reservaService = reservaService;
        }


        [HttpGet]
        public ActionResult Get(){
            return Ok(reservaService.Listar());
        }
        [HttpPost]
        public ActionResult Post([FromBody] Reserva reserva){
            return Ok(reservaService.Guardar(reserva));
        }
        [HttpPut]
        public ActionResult Put([FromBody] Reserva reserva){
            return Ok(reservaService.Actualizar(reserva));
        }
        [HttpDelete("{id}")]
        public ActionResult Delete(int id){
            return Ok(reservaService.Eliminar(id));
        }
        [HttpGet("id")]
        public ActionResult Get(int id){
            return Ok(reservaService.ListarPorId(id));
        }
    }
}