using Microsoft.AspNetCore.Mvc;
using GamePoint.Entity;
using GamePoint.Service;

namespace GamePoint.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class JuegoController: ControllerBase
    {
        private IJuegoService juegoService;
        public JuegoController(IJuegoService juegoService){
            this.juegoService = juegoService;
        }


        [HttpGet]
        public ActionResult Get(){
            return Ok(juegoService.Listar());
        }
        [HttpPost]
        public ActionResult Post([FromBody] Juego juego){
            return Ok(juegoService.Guardar(juego));
        }
        [HttpPut]
        public ActionResult Put([FromBody] Juego juego){
            return Ok(juegoService.Actualizar(juego));
        }
        [HttpDelete("{id}")]
        public ActionResult Delete(int id){
            return Ok(juegoService.Eliminar(id));
        }
        [HttpGet("id")]
        public ActionResult Get(int id){
            return Ok(juegoService.ListarPorId(id));
        }
        
    }
}