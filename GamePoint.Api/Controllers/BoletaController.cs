using Microsoft.AspNetCore.Mvc;
using GamePoint.Entity;
using GamePoint.Service;

namespace GamePoint.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BoletaController: ControllerBase
    {
        private IBoletaService boletaService;
        public BoletaController(IBoletaService boletaService){
            this.boletaService = boletaService;
        }


        [HttpGet]
        public ActionResult Get(){
            return Ok(boletaService.Listar());
        }
        [HttpPost]
        public ActionResult Post([FromBody] Boleta boleta){
            return Ok(boletaService.Guardar(boleta));
        }
        [HttpPut]
        public ActionResult Put([FromBody] Boleta boleta){
            return Ok(boletaService.Actualizar(boleta));
        }
        [HttpDelete("{id}")]
        public ActionResult Delete(int id){
            return Ok(boletaService.Eliminar(id));
        }
        [HttpGet("id")]
        public ActionResult Get(int id){
            return Ok(boletaService.ListarPorId(id));
        }

    }
}