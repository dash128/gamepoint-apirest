using Microsoft.AspNetCore.Mvc;
using GamePoint.Entity;
using GamePoint.Service;

namespace GamePoint.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MiembroController: ControllerBase
    {
        private IMiembroService miembroService;
        public MiembroController(IMiembroService miembroService){
            this.miembroService = miembroService;
        }


        [HttpGet]
        public ActionResult Get(){
            return Ok(miembroService.Listar());
        }
        [HttpPost]
        public ActionResult Post([FromBody] Miembro miembro){
            return Ok(miembroService.Guardar(miembro));
        }
        [HttpPut]
        public ActionResult Put([FromBody] Miembro miembro){
            return Ok(miembroService.Actualizar(miembro));
        }
        [HttpDelete("{id}")]
        public ActionResult Delete(int id){
            return Ok(miembroService.Eliminar(id));
        }
        [HttpGet("id")]
        public ActionResult Get(int id){
            return Ok(miembroService.ListarPorId(id));
        }
    }
}