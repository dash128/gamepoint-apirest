namespace GamePoint.Api
{
    public class SwaggerConfiguration
    {
        public const string EndpointDescription = "GamePoint API v1";
        public const string EndpointUrl = "/swagger/v1/swagger.json";
        public const string ContactName = "Sasuke Uchiha";
        public const string ContactUrl = "http://TeamFAS.com";
        public const string DocNameV1 = "v1";
        public const string DocInfoTitle = "GamePoint API";
        public const string DocInfoVersion = "v1";
        public const string DocInfoDescription = "GamePoint Api";
    }
}