﻿using GamePoint.Repository;
using GamePoint.Repository.Context;
using GamePoint.Repository.Implementation;
using GamePoint.Service;
using GamePoint.Service.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.Swagger;

namespace GamePoint.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options=>options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddTransient<IAmigoRepository,AmigoRepository> ();
            services.AddTransient<IAmigoService,AmigoService> ();
            services.AddTransient<IBoletaRepository,BoletaRepository> ();
            services.AddTransient<IBoletaService,BoletaService> ();
            services.AddTransient<ICabinaRepository,CabinaRepository> ();
            services.AddTransient<ICabinaService,CabinaService> ();
            services.AddTransient<IEquipoRepository,EquipoRepository> ();
            services.AddTransient<IEquipoService,EquipoService> ();
            services.AddTransient<IHistorialSuscripcionRepository,HistorialSuscripcionRepository> ();
            services.AddTransient<IHistorialSuscripcionService,HistorialSuscripcionService> ();
            services.AddTransient<IHorarioRepository,HorarioRepository> ();
            services.AddTransient<IHorarioService,HorarioService> ();
            services.AddTransient<IJuegoPorLanCenterRepository,JuegoPorLanCenterRepository> ();
            services.AddTransient<IJuegoPorLanCenterService,JuegoPorLanCenterService> ();
            services.AddTransient<IJuegoRepository,JuegoRepository> ();
            services.AddTransient<IJuegoService,JuegoService> ();
            services.AddTransient<IJugadorRepository,JugadorRepository> ();
            services.AddTransient<IJugadorService,JugadorService> ();
            services.AddTransient<ILanCenterRepository,LanCenterRepository> ();
            services.AddTransient<ILanCenterService,LanCenterService> ();
            services.AddTransient<IMiembroRepository,MiembroRepository> ();
            services.AddTransient<IMiembroService,MiembroService> ();
            services.AddTransient<IParticipanteRepository,ParticipanteRepository> ();
            services.AddTransient<IParticipanteService,ParticipanteService> ();
            services.AddTransient<IReservaRepository,ReservaRepository> ();
            services.AddTransient<IReservaService,ReservaService> ();
            services.AddTransient<IRetoRepository,RetoRepository> ();
            services.AddTransient<IRetoService,RetoService> ();
            services.AddTransient<ISuscripcionRepository,SuscripcionRepository> ();
            services.AddTransient<ISuscripcionService,SuscripcionService> ();
            services.AddTransient<ITorneoRepository,TorneoRepository> ();
            services.AddTransient<ITorneoService,TorneoService> ();


            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            // https://localhost:5001/swagger/index.html
            services.AddSwaggerGen (swagger => {
                var contact = new Contact () { Name = SwaggerConfiguration.ContactName, Url = SwaggerConfiguration.ContactUrl };
                swagger.SwaggerDoc (SwaggerConfiguration.DocNameV1,
                    new Info {
                        Title = SwaggerConfiguration.DocInfoTitle,
                            Version = SwaggerConfiguration.DocInfoVersion,
                            Description = SwaggerConfiguration.DocInfoDescription,
                            Contact = contact
                    }
                );
            });

            services.AddCors (options => {
                options.AddPolicy ("Todos",
                    builder => builder.WithOrigins ("*").WithHeaders ("*").WithMethods ("*"));
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseSwagger();
            app.UseSwaggerUI (c => {
                c.SwaggerEndpoint (SwaggerConfiguration.EndpointUrl, SwaggerConfiguration.EndpointDescription);
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseCors ("Todos");
            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
