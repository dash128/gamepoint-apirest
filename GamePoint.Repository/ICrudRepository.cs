using System.Collections.Generic;

namespace GamePoint.Repository
{
    public interface ICrudRepository<T>
    {
        bool Guardar(T entity);
        bool Actualizar(T entity);
        bool Eliminar(int id);
        IEnumerable<T> Listar();
        T ListarPorId(int id);
    }
}