using GamePoint.Entity;
using Microsoft.EntityFrameworkCore;

namespace GamePoint.Repository.Context
{
    public class ApplicationDbContext: DbContext
    {
        public DbSet<Amigo> Amigo {get; set;}
        public DbSet<Boleta> Boleta  {get; set;}
        public DbSet<Cabina> Cabina  {get; set;}
        public DbSet<Equipo> Equipo  {get; set;}
        public DbSet<HistorialSuscripcion> HistorialSuscripcion {get; set;}
        public DbSet<Horario> Horario {get; set;}
        public DbSet<Juego> Juego {get; set;}
        public DbSet<JuegoPorLanCenter> JuegoPorLanCenter {get; set;}
        public DbSet<Jugador> Jugador {get; set;}
        public DbSet<LanCenter> LanCenter {get; set;}
        public DbSet<Miembro> Miembro {get; set;}
        public DbSet<Participante> Participante {get; set;}
        public DbSet<Reserva> Reserva {get; set;}
        public DbSet<Reto> Reto {get; set;}
        public DbSet<Suscripcion> Suscripcion {get; set;}
        public DbSet<Torneo> Torneo {get; set;}


        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options):base(options){

        }
    }
}