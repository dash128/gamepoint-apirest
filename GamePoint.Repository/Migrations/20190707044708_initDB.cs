﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace GamePoint.Repository.Migrations
{
    public partial class initDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Horario",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FechaDia = table.Column<DateTime>(nullable: false),
                    HoraInicio = table.Column<int>(nullable: false),
                    Disponibilidad = table.Column<bool>(nullable: false),
                    LanCenterId = table.Column<int>(nullable: false),
                    CabinaId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Horario", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Juego",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Nombre = table.Column<string>(nullable: true),
                    Url = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Juego", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Jugador",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Nombre = table.Column<string>(nullable: true),
                    Celular = table.Column<string>(nullable: true),
                    Correo = table.Column<string>(nullable: true),
                    Contrasena = table.Column<string>(nullable: true),
                    FechaNacimiento = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Jugador", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "LanCenter",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Nombre = table.Column<string>(nullable: true),
                    Celular = table.Column<string>(nullable: true),
                    Correo = table.Column<string>(nullable: true),
                    Contrasena = table.Column<string>(nullable: true),
                    Distrito = table.Column<string>(nullable: true),
                    Latitud = table.Column<float>(nullable: false),
                    Longitud = table.Column<float>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LanCenter", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Suscripcion",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Nombre = table.Column<string>(nullable: true),
                    Duracion = table.Column<string>(nullable: true),
                    Precio = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Suscripcion", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Equipo",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Nombre = table.Column<string>(nullable: true),
                    FechaCreacion = table.Column<DateTime>(nullable: false),
                    JuegoId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Equipo", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Equipo_Juego_JuegoId",
                        column: x => x.JuegoId,
                        principalTable: "Juego",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Amigo",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    JugadorID1 = table.Column<int>(nullable: false),
                    Jugador1Id = table.Column<int>(nullable: true),
                    JugadorID2 = table.Column<int>(nullable: false),
                    Jugador2Id = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Amigo", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Amigo_Jugador_Jugador1Id",
                        column: x => x.Jugador1Id,
                        principalTable: "Jugador",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Amigo_Jugador_Jugador2Id",
                        column: x => x.Jugador2Id,
                        principalTable: "Jugador",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Reserva",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FechaReserva = table.Column<DateTime>(nullable: false),
                    Estado = table.Column<string>(nullable: true),
                    JugadorId = table.Column<int>(nullable: false),
                    HorarioId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Reserva", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Reserva_Horario_HorarioId",
                        column: x => x.HorarioId,
                        principalTable: "Horario",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Reserva_Jugador_JugadorId",
                        column: x => x.JugadorId,
                        principalTable: "Jugador",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Cabina",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Numero = table.Column<int>(nullable: false),
                    LanCenterId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cabina", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Cabina_LanCenter_LanCenterId",
                        column: x => x.LanCenterId,
                        principalTable: "LanCenter",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "JuegoPorLanCenter",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    LanCenterId = table.Column<int>(nullable: false),
                    JuegoId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JuegoPorLanCenter", x => x.Id);
                    table.ForeignKey(
                        name: "FK_JuegoPorLanCenter_Juego_JuegoId",
                        column: x => x.JuegoId,
                        principalTable: "Juego",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_JuegoPorLanCenter_LanCenter_LanCenterId",
                        column: x => x.LanCenterId,
                        principalTable: "LanCenter",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Torneo",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Nombre = table.Column<string>(nullable: true),
                    FechaComienzo = table.Column<DateTime>(nullable: false),
                    LanCenterId = table.Column<int>(nullable: false),
                    JuegoId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Torneo", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Torneo_Juego_JuegoId",
                        column: x => x.JuegoId,
                        principalTable: "Juego",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Torneo_LanCenter_LanCenterId",
                        column: x => x.LanCenterId,
                        principalTable: "LanCenter",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Boleta",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FechaMenbresia = table.Column<DateTime>(nullable: false),
                    Pago = table.Column<double>(nullable: false),
                    LanCenterId = table.Column<int>(nullable: false),
                    SuscripcionId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Boleta", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Boleta_LanCenter_LanCenterId",
                        column: x => x.LanCenterId,
                        principalTable: "LanCenter",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Boleta_Suscripcion_SuscripcionId",
                        column: x => x.SuscripcionId,
                        principalTable: "Suscripcion",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "HistorialSuscripcion",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FechaInicio = table.Column<DateTime>(nullable: false),
                    FechaFin = table.Column<DateTime>(nullable: false),
                    LanCenterId = table.Column<int>(nullable: false),
                    SuscripcionId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HistorialSuscripcion", x => x.Id);
                    table.ForeignKey(
                        name: "FK_HistorialSuscripcion_LanCenter_LanCenterId",
                        column: x => x.LanCenterId,
                        principalTable: "LanCenter",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_HistorialSuscripcion_Suscripcion_SuscripcionId",
                        column: x => x.SuscripcionId,
                        principalTable: "Suscripcion",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Miembro",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Capitan = table.Column<bool>(nullable: false),
                    EquipoId = table.Column<int>(nullable: false),
                    JugadorId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Miembro", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Miembro_Equipo_EquipoId",
                        column: x => x.EquipoId,
                        principalTable: "Equipo",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Miembro_Jugador_JugadorId",
                        column: x => x.JugadorId,
                        principalTable: "Jugador",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Participante",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Puesto = table.Column<int>(nullable: false),
                    Aceptado = table.Column<bool>(nullable: false),
                    TorneoId = table.Column<int>(nullable: false),
                    EquipoId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Participante", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Participante_Equipo_EquipoId",
                        column: x => x.EquipoId,
                        principalTable: "Equipo",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Reto",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Ganador = table.Column<int>(nullable: false),
                    EquipoID1 = table.Column<int>(nullable: false),
                    Equipo1Id = table.Column<int>(nullable: true),
                    EquipoID2 = table.Column<int>(nullable: false),
                    Equipo2Id = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Reto", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Reto_Equipo_Equipo1Id",
                        column: x => x.Equipo1Id,
                        principalTable: "Equipo",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Reto_Equipo_Equipo2Id",
                        column: x => x.Equipo2Id,
                        principalTable: "Equipo",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Amigo_Jugador1Id",
                table: "Amigo",
                column: "Jugador1Id");

            migrationBuilder.CreateIndex(
                name: "IX_Amigo_Jugador2Id",
                table: "Amigo",
                column: "Jugador2Id");

            migrationBuilder.CreateIndex(
                name: "IX_Boleta_LanCenterId",
                table: "Boleta",
                column: "LanCenterId");

            migrationBuilder.CreateIndex(
                name: "IX_Boleta_SuscripcionId",
                table: "Boleta",
                column: "SuscripcionId");

            migrationBuilder.CreateIndex(
                name: "IX_Cabina_LanCenterId",
                table: "Cabina",
                column: "LanCenterId");

            migrationBuilder.CreateIndex(
                name: "IX_Equipo_JuegoId",
                table: "Equipo",
                column: "JuegoId");

            migrationBuilder.CreateIndex(
                name: "IX_HistorialSuscripcion_LanCenterId",
                table: "HistorialSuscripcion",
                column: "LanCenterId");

            migrationBuilder.CreateIndex(
                name: "IX_HistorialSuscripcion_SuscripcionId",
                table: "HistorialSuscripcion",
                column: "SuscripcionId");

            migrationBuilder.CreateIndex(
                name: "IX_JuegoPorLanCenter_JuegoId",
                table: "JuegoPorLanCenter",
                column: "JuegoId");

            migrationBuilder.CreateIndex(
                name: "IX_JuegoPorLanCenter_LanCenterId",
                table: "JuegoPorLanCenter",
                column: "LanCenterId");

            migrationBuilder.CreateIndex(
                name: "IX_Miembro_EquipoId",
                table: "Miembro",
                column: "EquipoId");

            migrationBuilder.CreateIndex(
                name: "IX_Miembro_JugadorId",
                table: "Miembro",
                column: "JugadorId");

            migrationBuilder.CreateIndex(
                name: "IX_Participante_EquipoId",
                table: "Participante",
                column: "EquipoId");

            migrationBuilder.CreateIndex(
                name: "IX_Reserva_HorarioId",
                table: "Reserva",
                column: "HorarioId");

            migrationBuilder.CreateIndex(
                name: "IX_Reserva_JugadorId",
                table: "Reserva",
                column: "JugadorId");

            migrationBuilder.CreateIndex(
                name: "IX_Reto_Equipo1Id",
                table: "Reto",
                column: "Equipo1Id");

            migrationBuilder.CreateIndex(
                name: "IX_Reto_Equipo2Id",
                table: "Reto",
                column: "Equipo2Id");

            migrationBuilder.CreateIndex(
                name: "IX_Torneo_JuegoId",
                table: "Torneo",
                column: "JuegoId");

            migrationBuilder.CreateIndex(
                name: "IX_Torneo_LanCenterId",
                table: "Torneo",
                column: "LanCenterId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Amigo");

            migrationBuilder.DropTable(
                name: "Boleta");

            migrationBuilder.DropTable(
                name: "Cabina");

            migrationBuilder.DropTable(
                name: "HistorialSuscripcion");

            migrationBuilder.DropTable(
                name: "JuegoPorLanCenter");

            migrationBuilder.DropTable(
                name: "Miembro");

            migrationBuilder.DropTable(
                name: "Participante");

            migrationBuilder.DropTable(
                name: "Reserva");

            migrationBuilder.DropTable(
                name: "Reto");

            migrationBuilder.DropTable(
                name: "Torneo");

            migrationBuilder.DropTable(
                name: "Suscripcion");

            migrationBuilder.DropTable(
                name: "Horario");

            migrationBuilder.DropTable(
                name: "Jugador");

            migrationBuilder.DropTable(
                name: "Equipo");

            migrationBuilder.DropTable(
                name: "LanCenter");

            migrationBuilder.DropTable(
                name: "Juego");
        }
    }
}
