using System.Collections.Generic;
using System.Linq;
using GamePoint.Entity;
using GamePoint.Repository.Context;

namespace GamePoint.Repository.Implementation
{
    public class ParticipanteRepository : IParticipanteRepository
    {
        private ApplicationDbContext context;
        public ParticipanteRepository(ApplicationDbContext context){
            this.context = context;
        }


        public bool Actualizar(Participante entity)
        {
            try{
                var ParticipanteOrigen = context.Participante.Single(e => e.Id == entity.Id);

                ParticipanteOrigen.Id = entity.Id;
                ParticipanteOrigen.Puesto = entity.Puesto;
                ParticipanteOrigen.Aceptado = entity.Aceptado;

                ParticipanteOrigen.TorneoId = entity.TorneoId;
                ParticipanteOrigen.EquipoId = entity.EquipoId;

                context.Update(ParticipanteOrigen);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public bool Eliminar(int id)
        {
            throw new System.NotImplementedException();
        }

        public bool Guardar(Participante entity)
        {
            try{
                context.Add(entity);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public IEnumerable<Participante> Listar()
        {
            var resultado = new List<Participante>();

            try{
                resultado = context.Participante.ToList();
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public Participante ListarPorId(int id)
        {
            var resultado = new Participante();

            try{
                resultado = context.Participante.Single(e => e.Id == id);
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }
    }
}