using System.Collections.Generic;
using System.Linq;
using GamePoint.Entity;
using GamePoint.Repository.Context;
using Microsoft.EntityFrameworkCore;

namespace GamePoint.Repository.Implementation
{
    public class JuegoPorLanCenterRepository : IJuegoPorLanCenterRepository
    {
        private ApplicationDbContext context;
        public JuegoPorLanCenterRepository(ApplicationDbContext context){
            this.context = context;
        }


        public bool Actualizar(JuegoPorLanCenter entity)
        {
            try{
                var JuegoPorLanCenterOrigen = context.JuegoPorLanCenter.Single(e => e.Id == entity.Id);

                JuegoPorLanCenterOrigen.Id = entity.Id;
                
                JuegoPorLanCenterOrigen.LanCenterId = entity.LanCenterId;
                JuegoPorLanCenterOrigen.JuegoId = entity.JuegoId;

                context.Update(JuegoPorLanCenterOrigen);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public bool Eliminar(int id)
        {
            throw new System.NotImplementedException();
        }

        public bool Guardar(JuegoPorLanCenter entity)
        {
            try{
                context.Add(entity);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public IEnumerable<JuegoPorLanCenter> Listar()
        {
            var resultado = new List<JuegoPorLanCenter>();

            try{
                resultado = context.JuegoPorLanCenter.ToList();
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public IEnumerable<JuegoPorLanCenter> ListarJuegoPorLanCenter(int id)
        {
            var resultado = new List<JuegoPorLanCenter>();

            try{
                resultado = context.JuegoPorLanCenter.Include(j => j.Juego).Where(l => l.LanCenterId == id).ToList();
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public IEnumerable<JuegoPorLanCenter> ListarLanCenterPorJuego(int id)
        {
            var resultado = new List<JuegoPorLanCenter>();

            try{
                resultado = context.JuegoPorLanCenter.Include(j => j.LanCenter).Where(l => l.JuegoId == id).ToList();
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }
        public JuegoPorLanCenter ListarPorId(int id)
        {
            var resultado = new JuegoPorLanCenter();

            try{
                resultado = context.JuegoPorLanCenter.Single(e => e.Id == id);
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }
    }
}