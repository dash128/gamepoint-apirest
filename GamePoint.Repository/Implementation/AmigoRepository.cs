using System.Collections.Generic;
using System.Linq;
using GamePoint.Entity;
using GamePoint.Repository.Context;

namespace GamePoint.Repository.Implementation
{
    public class AmigoRepository : IAmigoRepository
    {
        private ApplicationDbContext context;
        public AmigoRepository(ApplicationDbContext context){
            this.context = context;
        }


        public bool Actualizar(Amigo entity)
        {
            try{
                var AmigoOrigen = context.Amigo.Single(e => e.Id == entity.Id);

                AmigoOrigen.Id = entity.Id;
                AmigoOrigen.JugadorID1 = entity.JugadorID1;
                AmigoOrigen.JugadorID2 = entity.JugadorID2;
                AmigoOrigen.Jugador1 = entity.Jugador1;
                AmigoOrigen.Jugador2 = entity.Jugador2;

                context.Update(AmigoOrigen);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public bool Eliminar(int id)
        {
            throw new System.NotImplementedException();
        }

        public bool Guardar(Amigo entity)
        {
            try{
                context.Add(entity);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public IEnumerable<Amigo> Listar()
        {
            var resultado = new List<Amigo>();

            try{
                resultado = context.Amigo.ToList();
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public Amigo ListarPorId(int id)
        {
            var resultado = new Amigo();

            try{
                resultado = context.Amigo.Single(e => e.Id == id);
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }
    }
}