using System.Collections.Generic;
using System.Linq;
using GamePoint.Entity;
using GamePoint.Repository.Context;

namespace GamePoint.Repository.Implementation
{
    public class BoletaRepository: IBoletaRepository
    {
        private ApplicationDbContext context;
        public BoletaRepository(ApplicationDbContext context){
            this.context = context;
        }

        public bool Actualizar(Boleta entity)
        {
            try{
                var BoletaOrigen = context.Boleta.Single(e => e.Id == entity.Id);

                BoletaOrigen.Id = entity.Id;
                BoletaOrigen.FechaMenbresia = entity.FechaMenbresia;
                BoletaOrigen.Pago = entity.Pago;
                
                BoletaOrigen.LanCenterId = entity.LanCenterId;
                BoletaOrigen.SuscripcionId = entity.SuscripcionId;

                context.Update(BoletaOrigen);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public bool Eliminar(int id)
        {
            throw new System.NotImplementedException();
        }

        public bool Guardar(Boleta entity)
        {
            try{
                context.Add(entity);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public IEnumerable<Boleta> Listar()
        {
            var resultado = new List<Boleta>();

            try{
                resultado = context.Boleta.ToList();
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public Boleta ListarPorId(int id)
        {
            var resultado = new Boleta();

            try{
                resultado = context.Boleta.Single(e => e.Id == id);
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }
    }
}