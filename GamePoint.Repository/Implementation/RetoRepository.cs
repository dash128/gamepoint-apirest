using System.Collections.Generic;
using System.Linq;
using GamePoint.Entity;
using GamePoint.Repository.Context;

namespace GamePoint.Repository.Implementation
{
    public class RetoRepository : IRetoRepository
    {
        private ApplicationDbContext context;
        public RetoRepository(ApplicationDbContext context){
            this.context = context;
        }


        public bool Actualizar(Reto entity)
        {
            try{
                var RetoOrigen = context.Reto.Single(e => e.Id == entity.Id);

                RetoOrigen.Id = entity.Id;
                RetoOrigen.Ganador = entity.Ganador;
               
                RetoOrigen.EquipoID1 = entity.EquipoID1;
                RetoOrigen.EquipoID2 = entity.EquipoID2;

                context.Update(RetoOrigen);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public bool Eliminar(int id)
        {
            throw new System.NotImplementedException();
        }

        public bool Guardar(Reto entity)
        {
             try{
                context.Add(entity);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public IEnumerable<Reto> Listar()
        {
            var resultado = new List<Reto>();

            try{
                resultado = context.Reto.ToList();
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public Reto ListarPorId(int id)
        {
            var resultado = new Reto();

            try{
                resultado = context.Reto.Single(e => e.Id == id);
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }
    }
}