using System.Collections.Generic;
using System.Linq;
using GamePoint.Entity;
using GamePoint.Repository.Context;

namespace GamePoint.Repository.Implementation
{
    public class CabinaRepository : ICabinaRepository
    {
        private ApplicationDbContext context;
        public CabinaRepository(ApplicationDbContext context){
            this.context = context;
        }


        public bool Actualizar(Cabina entity)
        {
            try{
                var CabinaOrigen = context.Cabina.Single(e => e.Id == entity.Id);

                CabinaOrigen.Id = entity.Id;
                CabinaOrigen.Numero = entity.Numero;

                CabinaOrigen.LanCenterId = entity.LanCenterId;

                context.Update(CabinaOrigen);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public bool Eliminar(int id)
        {
            throw new System.NotImplementedException();
        }

        public bool Guardar(Cabina entity)
        {
            try{
                context.Add(entity);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public IEnumerable<Cabina> Listar()
        {
            var resultado = new List<Cabina>();

            try{
                resultado = context.Cabina.ToList();
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public IEnumerable<Cabina> ListarCabinaPorLanCenter(int id)
        {
            var resultado = new List<Cabina>();

            try{
                resultado = context.Cabina.Where(c => c.LanCenterId == id).ToList();
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public Cabina ListarPorId(int id)
        {
            var resultado = new Cabina();

            try{
                resultado = context.Cabina.Single(e => e.Id == id);
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }
    }
}