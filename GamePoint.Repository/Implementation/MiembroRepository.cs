using System.Collections.Generic;
using System.Linq;
using GamePoint.Entity;
using GamePoint.Repository.Context;

namespace GamePoint.Repository.Implementation
{
    public class MiembroRepository : IMiembroRepository
    {
        private ApplicationDbContext context;
        public MiembroRepository(ApplicationDbContext context){
            this.context = context;
        }


        public bool Actualizar(Miembro entity)
        {
            try{
                var MiembroOrigen = context.Miembro.Single(e => e.Id == entity.Id);

                MiembroOrigen.Id = entity.Id;
                MiembroOrigen.Capitan = entity.Capitan;
     
                MiembroOrigen.EquipoId = entity.EquipoId;
                MiembroOrigen.JugadorId = entity.JugadorId;

                context.Update(MiembroOrigen);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public bool Eliminar(int id)
        {
            throw new System.NotImplementedException();
        }

        public bool Guardar(Miembro entity)
        {
            try{
                context.Add(entity);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public IEnumerable<Miembro> Listar()
        {
            var resultado = new List<Miembro>();

            try{
                resultado = context.Miembro.ToList();
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public Miembro ListarPorId(int id)
        {
            var resultado = new Miembro();

            try{
                resultado = context.Miembro.Single(e => e.Id == id);
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }
    }
}