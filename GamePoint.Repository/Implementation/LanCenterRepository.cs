using System.Collections.Generic;
using System.Linq;
using GamePoint.Entity;
using GamePoint.Repository.Context;
using Microsoft.EntityFrameworkCore;

namespace GamePoint.Repository.Implementation
{
    public class LanCenterRepository : ILanCenterRepository
    {
        private ApplicationDbContext context;
        public LanCenterRepository(ApplicationDbContext context){
            this.context = context;
        }


        public bool Actualizar(LanCenter entity)
        {
            try{
                var LanCenterOrigen = context.LanCenter.Single(e => e.Id == entity.Id);

                LanCenterOrigen.Id = entity.Id;
                LanCenterOrigen.Nombre = entity.Nombre;
                LanCenterOrigen.Celular = entity.Celular;
                LanCenterOrigen.Correo = entity.Correo;
                LanCenterOrigen.Contrasena = entity.Contrasena;
                LanCenterOrigen.Distrito = entity.Distrito;
                LanCenterOrigen.Latitud = entity.Latitud;
                LanCenterOrigen.Longitud = entity.Longitud;
                
                context.Update(LanCenterOrigen);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public bool Eliminar(int id)
        {
            throw new System.NotImplementedException();
        }

        public bool Guardar(LanCenter entity)
        {
            try{
                context.Add(entity);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public IEnumerable<LanCenter> Listar()
        {
            var resultado = new List<LanCenter>();

            try{
                resultado = context.LanCenter.ToList();
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public IEnumerable<LanCenter> ListarConDistrito(string distrito)
        {
            var resultado = new List<LanCenter>();
            try{
                resultado = context.LanCenter.Where(l => l.Distrito.Contains(distrito)).ToList();
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public IEnumerable<LanCenter> ListarConNombre(string nombre)
        {
            var resultado = new List<LanCenter>();
            try{
                resultado = context.LanCenter.Where(l => l.Nombre.Contains(nombre)).ToList();
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public LanCenter ListarPorId(int id)
        {
            var resultado = new LanCenter();

            try{
                resultado = context.LanCenter.Single(e => e.Id == id);
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }
    }
}