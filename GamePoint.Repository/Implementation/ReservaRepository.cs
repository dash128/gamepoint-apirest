using System.Collections.Generic;
using System.Linq;
using GamePoint.Entity;
using GamePoint.Repository.Context;

namespace GamePoint.Repository.Implementation
{
    public class ReservaRepository : IReservaRepository
    {
        private ApplicationDbContext context;
        public ReservaRepository(ApplicationDbContext context){
            this.context = context;
        }


        public bool Actualizar(Reserva entity)
        {
            try{
                var ReservaOrigen = context.Reserva.Single(e => e.Id == entity.Id);

                ReservaOrigen.Id = entity.Id;
                ReservaOrigen.FechaReserva = entity.FechaReserva;
                ReservaOrigen.Estado = entity.Estado;

                ReservaOrigen.JugadorId = entity.JugadorId;
                ReservaOrigen.HorarioId = entity.HorarioId;

                context.Update(ReservaOrigen);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public bool Eliminar(int id)
        {
            throw new System.NotImplementedException();
        }

        public bool Guardar(Reserva entity)
        {
            try{
                context.Add(entity);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public IEnumerable<Reserva> Listar()
        {
            var resultado = new List<Reserva>();

            try{
                resultado = context.Reserva.ToList();
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public Reserva ListarPorId(int id)
        {
            var resultado = new Reserva();

            try{
                resultado = context.Reserva.Single(e => e.Id == id);
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }
    }
}