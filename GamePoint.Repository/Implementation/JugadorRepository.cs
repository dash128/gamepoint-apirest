using System.Collections.Generic;
using System.Linq;
using GamePoint.Entity;
using GamePoint.Repository.Context;
using GamePoint.Repository.Dto;

namespace GamePoint.Repository.Implementation
{
    public class JugadorRepository : IJugadorRepository
    {
        private ApplicationDbContext context;
        public JugadorRepository(ApplicationDbContext context){
            this.context = context;
        }
        
        public bool Actualizar(Jugador entity)
        {
            try{
                var jugadorOrigen = context.Jugador.Single(e => e.Id == entity.Id);

                jugadorOrigen.Id = entity.Id;
                jugadorOrigen.Nombre = entity.Nombre;
                jugadorOrigen.Celular = entity.Celular;
                jugadorOrigen.Correo = entity.Correo;
                jugadorOrigen.Contrasena = entity.Contrasena;
                jugadorOrigen.FechaNacimiento = entity.FechaNacimiento;

                context.Update(jugadorOrigen);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public bool Eliminar(int id)
        {
            throw new System.NotImplementedException();
        }

        public bool Guardar(Jugador entity)
        {
            try{
                context.Add(entity);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public IEnumerable<Jugador> Listar()
        {
            var resultado = new List<Jugador>();

            try{
                resultado = context.Jugador.ToList();
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public Jugador ListarPorId(int id)
        {
            var resultado = new Jugador();

            try{
                resultado = context.Jugador.Single(e => e.Id == id);
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public object ValidarCorreo(string correo, string contrasena)
        {
            var resultadoDTO = new UserAccount();
            var jugador = new Jugador();
            try{
                jugador = context.Jugador.Single(p => p.Correo == correo && p.Contrasena == contrasena);
                resultadoDTO.Id = jugador.Id;
                resultadoDTO.Correo = jugador.Correo;
                resultadoDTO.Nombre = jugador.Nombre;
            }catch(System.Exception){
                throw;
            }
            return resultadoDTO;
        }


    }
}