using System;
using System.Collections.Generic;
using System.Linq;
using GamePoint.Entity;
using GamePoint.Repository.Context;
using Microsoft.EntityFrameworkCore;

namespace GamePoint.Repository.Implementation
{
    public class HorarioRepository : IHorarioRepository
    {
        private ApplicationDbContext context;
        public HorarioRepository(ApplicationDbContext context){
            this.context = context;
        }


        public bool Actualizar(Horario entity)
        {
            try{
                var HorarioOrigen = context.Horario.Single(e => e.Id == entity.Id);

                HorarioOrigen.Id = entity.Id;
                HorarioOrigen.FechaDia = entity.FechaDia;
                HorarioOrigen.HoraInicio = entity.HoraInicio;
                HorarioOrigen.Disponibilidad = entity.Disponibilidad;

                HorarioOrigen.LanCenterId = entity.LanCenterId;

                context.Update(HorarioOrigen);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public bool Eliminar(int id)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<Horario> GetCabinaHorarioPorLan(int idC, int idL)
        {
             DateTime dia = DateTime.Now;
             
            var resultado = new List<Horario>();

            try{
                resultado = context.Horario.Where(l => l.CabinaId == idC  ).Where(h =>h.LanCenterId == idL).Where(f =>f.FechaDia.Year.ToString() == dia.Year.ToString() && f.FechaDia.Month.ToString() == dia.Month.ToString() && f.FechaDia.Day.ToString() == dia.Day.ToString()).ToList();
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public bool Guardar(Horario entity)
        {
            try{
                context.Add(entity);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public IEnumerable<Horario> Listar()
        {
            var resultado = new List<Horario>();

            try{
                resultado = context.Horario.ToList();
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public Horario ListarPorId(int id)
        {
            var resultado = new Horario();

            try{
                resultado = context.Horario.Single(e => e.Id == id);
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }
    }
}