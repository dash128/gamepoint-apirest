using System.Collections.Generic;
using System.Linq;
using GamePoint.Entity;
using GamePoint.Repository.Context;

namespace GamePoint.Repository.Implementation
{
    public class HistorialSuscripcionRepository : IHistorialSuscripcionRepository
    {
        private ApplicationDbContext context;
        public HistorialSuscripcionRepository(ApplicationDbContext context){
            this.context = context;
        }


        public bool Actualizar(HistorialSuscripcion entity)
        {
            try{
                var HistorialSuscripcionOrigen = context.HistorialSuscripcion.Single(e => e.Id == entity.Id);

                HistorialSuscripcionOrigen.Id = entity.Id;
                HistorialSuscripcionOrigen.FechaInicio = entity.FechaInicio;
                HistorialSuscripcionOrigen.FechaFin = entity.FechaFin;

                HistorialSuscripcionOrigen.LanCenterId = entity.LanCenterId;
                HistorialSuscripcionOrigen.SuscripcionId = entity.SuscripcionId;

                context.Update(HistorialSuscripcionOrigen);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public bool Eliminar(int id)
        {
            throw new System.NotImplementedException();
        }

        public bool Guardar(HistorialSuscripcion entity)
        {
            try{
                context.Add(entity);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public IEnumerable<HistorialSuscripcion> Listar()
        {
            var resultado = new List<HistorialSuscripcion>();

            try{
                resultado = context.HistorialSuscripcion.ToList();
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public HistorialSuscripcion ListarPorId(int id)
        {
            var resultado = new HistorialSuscripcion();

            try{
                resultado = context.HistorialSuscripcion.Single(e => e.Id == id);
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }
    }
}