using System.Collections.Generic;
using System.Linq;
using GamePoint.Entity;
using GamePoint.Repository.Context;

namespace GamePoint.Repository.Implementation
{
    public class TorneoRepository : ITorneoRepository
    {
        private ApplicationDbContext context;
        public TorneoRepository(ApplicationDbContext context){
            this.context = context;
        }


        public bool Actualizar(Torneo entity)
        {
            try{
                var TorneoOrigen = context.Torneo.Single(e => e.Id == entity.Id);

                TorneoOrigen.Id = entity.Id;
                TorneoOrigen.Nombre = entity.Nombre;
                TorneoOrigen.FechaComienzo = entity.FechaComienzo;

                TorneoOrigen.LanCenterId = entity.LanCenterId;
                TorneoOrigen.JuegoId = entity.JuegoId;

                context.Update(TorneoOrigen);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public bool Eliminar(int id)
        {
            throw new System.NotImplementedException();
        }

        public bool Guardar(Torneo entity)
        {
            try{
                context.Add(entity);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public IEnumerable<Torneo> Listar()
        {
            var resultado = new List<Torneo>();

            try{
                resultado = context.Torneo.ToList();
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public Torneo ListarPorId(int id)
        {
            var resultado = new Torneo();

            try{
                resultado = context.Torneo.Single(e => e.Id == id);
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }
    }
}