using System.Collections.Generic;
using System.Linq;
using GamePoint.Entity;
using GamePoint.Repository.Context;
using GamePoint.Repository.Dto;

namespace GamePoint.Repository.Implementation
{
    public class JuegoRepository : IJuegoRepository
    {
        private ApplicationDbContext context;
        public JuegoRepository(ApplicationDbContext context){
            this.context = context;
        }


        public bool Actualizar(Juego entity)
        {
            try{
                var JuegoOrigen = context.Juego.Single(e => e.Id == entity.Id);

                JuegoOrigen.Id = entity.Id;
                JuegoOrigen.Nombre = entity.Nombre;
                JuegoOrigen.Url = entity.Url;

                context.Update(JuegoOrigen);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public bool Eliminar(int id)
        {
            throw new System.NotImplementedException();
        }

        public bool Guardar(Juego entity)
        {
            try{
                context.Add(entity);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public IEnumerable<Juego> Listar()
        {
            var resultado = new List<Juego>();

            try{
                resultado = context.Juego.ToList();
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public Juego ListarPorId(int id)
        {
            var resultado = new Juego();

            try{
                resultado = context.Juego.Single(e => e.Id == id);
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        
    }
}