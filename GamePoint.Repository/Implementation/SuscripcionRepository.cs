using System.Collections.Generic;
using System.Linq;
using GamePoint.Entity;
using GamePoint.Repository.Context;

namespace GamePoint.Repository.Implementation
{
    public class SuscripcionRepository : ISuscripcionRepository
    {
        private ApplicationDbContext context;
        public SuscripcionRepository(ApplicationDbContext context){
            this.context = context;
        }


        public bool Actualizar(Suscripcion entity)
        {
            try{
                var SuscripcionOrigen = context.Suscripcion.Single(e => e.Id == entity.Id);

                SuscripcionOrigen.Id = entity.Id;
                SuscripcionOrigen.Nombre = entity.Nombre;
                SuscripcionOrigen.Duracion = entity.Duracion;
                SuscripcionOrigen.Precio = entity.Precio;
                

                context.Update(SuscripcionOrigen);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public bool Eliminar(int id)
        {
            throw new System.NotImplementedException();
        }

        public bool Guardar(Suscripcion entity)
        {
            try{
                context.Add(entity);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public IEnumerable<Suscripcion> Listar()
        {
            var resultado = new List<Suscripcion>();

            try{
                resultado = context.Suscripcion.ToList();
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public Suscripcion ListarPorId(int id)
        {
            var resultado = new Suscripcion();

            try{
                resultado = context.Suscripcion.Single(e => e.Id == id);
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }
    }
}