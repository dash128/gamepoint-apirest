using System.Collections.Generic;
using System.Linq;
using GamePoint.Entity;
using GamePoint.Repository.Context;

namespace GamePoint.Repository.Implementation
{
    public class EquipoRepository : IEquipoRepository
    {
        private ApplicationDbContext context;
        public EquipoRepository(ApplicationDbContext context){
            this.context = context;
        }


        public bool Actualizar(Equipo entity)
        {
            try{
                var EquipoOrigen = context.Equipo.Single(e => e.Id == entity.Id);

                EquipoOrigen.Id = entity.Id;
                EquipoOrigen.Nombre = entity.Nombre;
                EquipoOrigen.FechaCreacion = entity.FechaCreacion;

                EquipoOrigen.JuegoId = entity.JuegoId;

                context.Update(EquipoOrigen);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public bool Eliminar(int id)
        {
            throw new System.NotImplementedException();
        }

        public bool Guardar(Equipo entity)
        {
            try{
                context.Add(entity);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public IEnumerable<Equipo> Listar()
        {
            var resultado = new List<Equipo>();

            try{
                resultado = context.Equipo.ToList();
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public Equipo ListarPorId(int id)
        {
            var resultado = new Equipo();

            try{
                resultado = context.Equipo.Single(e => e.Id == id);
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }
    }
}