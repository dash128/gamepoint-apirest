using System.Collections.Generic;
using GamePoint.Entity;

namespace GamePoint.Repository
{
    public interface IJuegoPorLanCenterRepository: ICrudRepository<JuegoPorLanCenter>
    {
         IEnumerable<JuegoPorLanCenter> ListarJuegoPorLanCenter(int id);
         
         IEnumerable<JuegoPorLanCenter> ListarLanCenterPorJuego(int id);
    }
}