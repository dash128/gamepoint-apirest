using GamePoint.Entity;

namespace GamePoint.Repository
{
    public interface IParticipanteRepository: ICrudRepository<Participante>
    {
         
    }
}