using System.Collections.Generic;
using GamePoint.Entity;

namespace GamePoint.Repository
{
    public interface ILanCenterRepository: ICrudRepository<LanCenter>
    {
         IEnumerable<LanCenter> ListarConNombre(string nombre);
         IEnumerable<LanCenter> ListarConDistrito(string distrito);
    }
}