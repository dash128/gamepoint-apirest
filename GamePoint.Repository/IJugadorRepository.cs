using GamePoint.Entity;

namespace GamePoint.Repository
{
    public interface IJugadorRepository: ICrudRepository<Jugador>
    {
        object ValidarCorreo(string correo, string contrasena);
        
    }
}