using System.Collections.Generic;
using GamePoint.Entity;

namespace GamePoint.Repository
{
    public interface ICabinaRepository: ICrudRepository<Cabina>
    {
         IEnumerable<Cabina> ListarCabinaPorLanCenter(int id);
    }
}