using System.Collections.Generic;
using GamePoint.Entity;

namespace GamePoint.Repository
{
    public interface IHorarioRepository: ICrudRepository<Horario>
    {
         IEnumerable<Horario> GetCabinaHorarioPorLan(int idC, int idL);
    }
}