using System.Collections.Generic;
using GamePoint.Entity;

namespace GamePoint.Service
{
    public interface IJuegoPorLanCenterService: ICrudService<JuegoPorLanCenter>
    {
         
         IEnumerable<JuegoPorLanCenter> ListarJuegoPorLanCenter(int id);

         IEnumerable<JuegoPorLanCenter> ListarLanCenterPorJuego(int id);
    }
}