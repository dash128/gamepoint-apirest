using System.Collections.Generic;
using GamePoint.Entity;
using GamePoint.Repository;

namespace GamePoint.Service.Implementation
{
    public class TorneoService : ITorneoService
    {
        private ITorneoRepository torneoRepository;
        public TorneoService(ITorneoRepository torneoRepository){
            this.torneoRepository=torneoRepository;
        }


        public bool Actualizar(Torneo entity)
        {
            return torneoRepository.Actualizar(entity);
        }

        public bool Eliminar(int id)
        {
            throw new System.NotImplementedException();
        }

        public bool Guardar(Torneo entity)
        {
            return torneoRepository.Guardar(entity);
        }

        public IEnumerable<Torneo> Listar()
        {
            return torneoRepository.Listar();
        }

        public Torneo ListarPorId(int id)
        {
            return torneoRepository.ListarPorId(id);
        }
    }
}