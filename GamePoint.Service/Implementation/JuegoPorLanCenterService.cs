using System.Collections.Generic;
using GamePoint.Entity;
using GamePoint.Repository;

namespace GamePoint.Service.Implementation
{
    public class JuegoPorLanCenterService : IJuegoPorLanCenterService
    {
        private IJuegoPorLanCenterRepository juegoPorLanCenterRepository;
        public JuegoPorLanCenterService(IJuegoPorLanCenterRepository juegoPorLanCenterRepository){
            this.juegoPorLanCenterRepository=juegoPorLanCenterRepository;
        }

        
        public bool Actualizar(JuegoPorLanCenter entity)
        {
            return juegoPorLanCenterRepository.Actualizar(entity);
        }

        public bool Eliminar(int id)
        {
            throw new System.NotImplementedException();
        }

        public bool Guardar(JuegoPorLanCenter entity)
        {
            return juegoPorLanCenterRepository.Guardar(entity);
        }

        public IEnumerable<JuegoPorLanCenter> Listar()
        {
            return juegoPorLanCenterRepository.Listar();
        }

        public IEnumerable<JuegoPorLanCenter> ListarJuegoPorLanCenter(int id)
        {
            return juegoPorLanCenterRepository.ListarJuegoPorLanCenter(id);
        }

        public IEnumerable<JuegoPorLanCenter> ListarLanCenterPorJuego(int id)
        {
            return juegoPorLanCenterRepository.ListarLanCenterPorJuego(id);
        }

        public JuegoPorLanCenter ListarPorId(int id)
        {
            return juegoPorLanCenterRepository.ListarPorId(id);
        }
    }
}