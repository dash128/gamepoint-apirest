using System.Collections.Generic;
using GamePoint.Entity;
using GamePoint.Repository;

namespace GamePoint.Service.Implementation
{
    public class BoletaService : IBoletaService
    {
        private IBoletaRepository boletaRepository;
        public BoletaService(IBoletaRepository boletaRepository){
            this.boletaRepository = boletaRepository;
        }


        public bool Actualizar(Boleta entity)
        {
            return boletaRepository.Actualizar(entity);
        }

        public bool Eliminar(int id)
        {
            throw new System.NotImplementedException();
        }

        public bool Guardar(Boleta entity)
        {
            return boletaRepository.Guardar(entity);
        }

        public IEnumerable<Boleta> Listar()
        {
            return boletaRepository.Listar();
        }

        public Boleta ListarPorId(int id)
        {
            return boletaRepository.ListarPorId(id);
        }
    }
}