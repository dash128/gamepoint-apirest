using System.Collections.Generic;
using GamePoint.Entity;
using GamePoint.Repository;

namespace GamePoint.Service.Implementation
{
    public class ReservaService : IReservaService
    {
        private IReservaRepository reservaRepository;
        public ReservaService(IReservaRepository reservaRepository){
            this.reservaRepository=reservaRepository;
        }


        public bool Actualizar(Reserva entity)
        {
            return reservaRepository.Actualizar(entity);
        }

        public bool Eliminar(int id)
        {
            throw new System.NotImplementedException();
        }

        public bool Guardar(Reserva entity)
        {
            return reservaRepository.Guardar(entity);
        }

        public IEnumerable<Reserva> Listar()
        {
            return reservaRepository.Listar();
        }

        public Reserva ListarPorId(int id)
        {
            return reservaRepository.ListarPorId(id);
        }
    }
}