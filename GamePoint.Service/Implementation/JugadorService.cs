using System.Collections.Generic;
using GamePoint.Entity;
using GamePoint.Repository;

namespace GamePoint.Service.Implementation
{
    public class JugadorService : IJugadorService
    {
        private IJugadorRepository jugadorRepository;
        public JugadorService(IJugadorRepository jugadorRepository){
            this.jugadorRepository=jugadorRepository;
        }


        public bool Actualizar(Jugador entity)
        {
            return jugadorRepository.Actualizar(entity);
        }

        public bool Eliminar(int id)
        {
            return jugadorRepository.Eliminar(id);
        }

        public bool Guardar(Jugador entity)
        {
            return jugadorRepository.Guardar(entity);
        }

        public IEnumerable<Jugador> Listar()
        {
            return jugadorRepository.Listar();
        }

        public Jugador ListarPorId(int id)
        {
            return jugadorRepository.ListarPorId(id);
        }

        public object ValidarCorreo(string correo, string contrasena)
        {
            return jugadorRepository.ValidarCorreo(correo, contrasena);
        }
    }
}