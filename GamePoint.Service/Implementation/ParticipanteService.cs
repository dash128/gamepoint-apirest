using System.Collections.Generic;
using GamePoint.Entity;
using GamePoint.Repository;

namespace GamePoint.Service.Implementation
{
    public class ParticipanteService : IParticipanteService
    {
        private IParticipanteRepository participanteRepository;
        public ParticipanteService(IParticipanteRepository participanteRepository){
            this.participanteRepository=participanteRepository;
        }


        public bool Actualizar(Participante entity)
        {
            return participanteRepository.Actualizar(entity);
        }

        public bool Eliminar(int id)
        {
            throw new System.NotImplementedException();
        }

        public bool Guardar(Participante entity)
        {
            return participanteRepository.Guardar(entity);
        }

        public IEnumerable<Participante> Listar()
        {
            return participanteRepository.Listar();
        }

        public Participante ListarPorId(int id)
        {
            return participanteRepository.ListarPorId(id);
        }
    }
}