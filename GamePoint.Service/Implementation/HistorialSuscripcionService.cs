using System.Collections.Generic;
using GamePoint.Entity;
using GamePoint.Repository;

namespace GamePoint.Service.Implementation
{
    public class HistorialSuscripcionService : IHistorialSuscripcionService
    {
        private IHistorialSuscripcionRepository historialSuscripcionRepository;
        public HistorialSuscripcionService(IHistorialSuscripcionRepository historialSuscripcionRepository){
            this.historialSuscripcionRepository=historialSuscripcionRepository;
        }


        public bool Actualizar(HistorialSuscripcion entity)
        {
            return historialSuscripcionRepository.Actualizar(entity);
        }

        public bool Eliminar(int id)
        {
            throw new System.NotImplementedException();
        }

        public bool Guardar(HistorialSuscripcion entity)
        {
            return historialSuscripcionRepository.Guardar(entity);
        }

        public IEnumerable<HistorialSuscripcion> Listar()
        {
            return historialSuscripcionRepository.Listar();
        }

        public HistorialSuscripcion ListarPorId(int id)
        {
            return historialSuscripcionRepository.ListarPorId(id);
        }
    }
}