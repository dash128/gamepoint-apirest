using System.Collections.Generic;
using GamePoint.Entity;
using GamePoint.Repository;

namespace GamePoint.Service.Implementation
{
    public class LanCenterService : ILanCenterService
    {
        private ILanCenterRepository lanCenterRepository;
        public LanCenterService(ILanCenterRepository lanCenterRepository){
            this.lanCenterRepository=lanCenterRepository;
        }


        public bool Actualizar(LanCenter entity)
        {
            return lanCenterRepository.Actualizar(entity);
        }

        public bool Eliminar(int id)
        {
            throw new System.NotImplementedException();
        }

        public bool Guardar(LanCenter entity)
        {
            return lanCenterRepository.Guardar(entity);
        }

        public IEnumerable<LanCenter> Listar()
        {
            return lanCenterRepository.Listar();
        }

        public IEnumerable<LanCenter> ListarConDistrito(string distrito)
        {
            return lanCenterRepository.ListarConDistrito(distrito);
        }

        public IEnumerable<LanCenter> ListarConNombre(string nombre)
        {
            return lanCenterRepository.ListarConNombre(nombre);
        }

        public LanCenter ListarPorId(int id)
        {
            return lanCenterRepository.ListarPorId(id);
        }
    }
}