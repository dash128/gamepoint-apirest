using System.Collections.Generic;
using GamePoint.Entity;
using GamePoint.Repository;

namespace GamePoint.Service.Implementation
{
    public class MiembroService : IMiembroService
    {
        private IMiembroRepository miembroRepository;
        public MiembroService(IMiembroRepository miembroRepository){
            this.miembroRepository=miembroRepository;
        }


        public bool Actualizar(Miembro entity)
        {
            return miembroRepository.Actualizar(entity);
        }

        public bool Eliminar(int id)
        {
            throw new System.NotImplementedException();
        }

        public bool Guardar(Miembro entity)
        {
            return miembroRepository.Guardar(entity);
        }

        public IEnumerable<Miembro> Listar()
        {
            return miembroRepository.Listar();
        }

        public Miembro ListarPorId(int id)
        {
            return miembroRepository.ListarPorId(id);
        }
    }
}