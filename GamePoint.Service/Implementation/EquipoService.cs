using System.Collections.Generic;
using GamePoint.Entity;
using GamePoint.Repository;

namespace GamePoint.Service.Implementation
{
    public class EquipoService : IEquipoService
    {
        private IEquipoRepository equipoRepository;
        public EquipoService(IEquipoRepository equipoRepository){
            this.equipoRepository=equipoRepository;
        }


        public bool Actualizar(Equipo entity)
        {
            return equipoRepository.Actualizar(entity);
        }

        public bool Eliminar(int id)
        {
            throw new System.NotImplementedException();
        }

        public bool Guardar(Equipo entity)
        {
            return equipoRepository.Guardar(entity);
        }

        public IEnumerable<Equipo> Listar()
        {
            return equipoRepository.Listar();
        }

        public Equipo ListarPorId(int id)
        {
            return equipoRepository.ListarPorId(id);
        }
    }
}