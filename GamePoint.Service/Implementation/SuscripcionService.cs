using System.Collections.Generic;
using GamePoint.Entity;
using GamePoint.Repository;

namespace GamePoint.Service.Implementation
{
    public class SuscripcionService : ISuscripcionService
    {
        private ISuscripcionRepository suscripcionRepository;
        public SuscripcionService(ISuscripcionRepository suscripcionRepository){
            this.suscripcionRepository=suscripcionRepository;
        }


        public bool Actualizar(Suscripcion entity)
        {
            return suscripcionRepository.Actualizar(entity);
        }

        public bool Eliminar(int id)
        {
            throw new System.NotImplementedException();
        }

        public bool Guardar(Suscripcion entity)
        {
            return suscripcionRepository.Guardar(entity);
        }

        public IEnumerable<Suscripcion> Listar()
        {
            return suscripcionRepository.Listar();
        }

        public Suscripcion ListarPorId(int id)
        {
            return suscripcionRepository.ListarPorId(id);
        }
    }
}