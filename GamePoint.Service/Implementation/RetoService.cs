using System.Collections.Generic;
using GamePoint.Entity;
using GamePoint.Repository;

namespace GamePoint.Service.Implementation
{
    public class RetoService : IRetoService
    {
        private IRetoRepository retoRepository;
        public RetoService(IRetoRepository retoRepository){
            this.retoRepository=retoRepository;
        }

        
        public bool Actualizar(Reto entity)
        {
            return retoRepository.Actualizar(entity);
        }

        public bool Eliminar(int id)
        {
            throw new System.NotImplementedException();
        }

        public bool Guardar(Reto entity)
        {
            return retoRepository.Guardar(entity);
        }

        public IEnumerable<Reto> Listar()
        {
            return retoRepository.Listar();
        }

        public Reto ListarPorId(int id)
        {
            return retoRepository.ListarPorId(id);
        }
    }
}