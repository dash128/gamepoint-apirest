using System.Collections.Generic;
using GamePoint.Entity;
using GamePoint.Repository;

namespace GamePoint.Service.Implementation
{
    public class JuegoService : IJuegoService
    {
        private IJuegoRepository juegoRepository;
        public JuegoService(IJuegoRepository juegoRepository){
            this.juegoRepository=juegoRepository;
        }


        public bool Actualizar(Juego entity)
        {
            return juegoRepository.Actualizar(entity);
        }

        public bool Eliminar(int id)
        {
            throw new System.NotImplementedException();
        }

        public bool Guardar(Juego entity)
        {
            return juegoRepository.Guardar(entity);
        }

        public IEnumerable<Juego> Listar()
        {
            return juegoRepository.Listar();
        }

        public Juego ListarPorId(int id)
        {
            return juegoRepository.ListarPorId(id);
        }
    }
}