using System.Collections.Generic;
using GamePoint.Entity;
using GamePoint.Repository;

namespace GamePoint.Service.Implementation
{
    public class AmigoService : IAmigoService
    {
       private IAmigoRepository amigoRepository;
       public AmigoService(IAmigoRepository amigoRepository){
           this.amigoRepository = amigoRepository;
       }


        public bool Actualizar(Amigo entity)
        {
            return amigoRepository.Actualizar(entity);
        }

        public bool Eliminar(int id)
        {
            throw new System.NotImplementedException();
        }

        public bool Guardar(Amigo entity)
        {
            return amigoRepository.Guardar(entity);
        }

        public IEnumerable<Amigo> Listar()
        {
            return amigoRepository.Listar();
        }

        public Amigo ListarPorId(int id)
        {
            return amigoRepository.ListarPorId(id);
        }
    }
}