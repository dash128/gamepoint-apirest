using System.Collections.Generic;
using GamePoint.Entity;
using GamePoint.Repository;

namespace GamePoint.Service.Implementation
{
    public class CabinaService : ICabinaService
    {
        private ICabinaRepository cabinaRepository;
        public CabinaService(ICabinaRepository cabinaRepository){
            this.cabinaRepository=cabinaRepository;
        }


        public bool Actualizar(Cabina entity)
        {
            return cabinaRepository.Actualizar(entity);
        }

        public bool Eliminar(int id)
        {
            throw new System.NotImplementedException();
        }

        public bool Guardar(Cabina entity)
        {
            return cabinaRepository.Guardar(entity);
        }

        public IEnumerable<Cabina> Listar()
        {
            return cabinaRepository.Listar();
        }

        public IEnumerable<Cabina> ListarCabinaPorLanCenter(int id)
        {
            return cabinaRepository.ListarCabinaPorLanCenter(id);
        }

        public Cabina ListarPorId(int id)
        {
            return cabinaRepository.ListarPorId(id);
        }
    }
}