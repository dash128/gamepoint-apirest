using System.Collections.Generic;
using GamePoint.Entity;
using GamePoint.Repository;

namespace GamePoint.Service.Implementation
{
    public class HorarioService : IHorarioService
    {
        private IHorarioRepository horarioRepository;
        public HorarioService(IHorarioRepository horarioRepository){
            this.horarioRepository=horarioRepository;
        }

        
        public bool Actualizar(Horario entity)
        {
            return horarioRepository.Actualizar(entity);
        }

        public bool Eliminar(int id)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<Horario> GetCabinaHorarioPorLan(int idC, int idL)
        {
            return horarioRepository.GetCabinaHorarioPorLan(idC, idL);
        }

        public bool Guardar(Horario entity)
        {
            return horarioRepository.Guardar(entity);
        }

        public IEnumerable<Horario> Listar()
        {
            return horarioRepository.Listar();
        }

        public Horario ListarPorId(int id)
        {
            return horarioRepository.ListarPorId(id);
        }
    }
}