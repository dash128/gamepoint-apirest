using System.Collections.Generic;
using GamePoint.Entity;

namespace GamePoint.Service
{
    public interface ILanCenterService: ICrudService<LanCenter>
    {
         IEnumerable<LanCenter> ListarConNombre(string nombre);
         IEnumerable<LanCenter> ListarConDistrito(string distrito);
    }
}