using System.Collections.Generic;
using GamePoint.Entity;

namespace GamePoint.Service
{
    public interface IHorarioService: ICrudService<Horario>
    {

        IEnumerable<Horario> GetCabinaHorarioPorLan(int idC, int idL);
    }
}