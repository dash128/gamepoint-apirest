using GamePoint.Entity;

namespace GamePoint.Service
{
    public interface IJugadorService: ICrudService<Jugador>
    {
         object ValidarCorreo(string correo, string contrasena);
    }
}