using System.Collections.Generic;
using GamePoint.Entity;

namespace GamePoint.Service
{
    public interface ICabinaService: ICrudService<Cabina>
    {
         IEnumerable<Cabina> ListarCabinaPorLanCenter(int id);
    }
}