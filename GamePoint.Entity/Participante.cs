namespace GamePoint.Entity
{
    public class Participante
    {
        public int Id {get; set;}
        public int Puesto {get; set;}
        public bool Aceptado {get; set;}


        public int TorneoId {get; set;}
        // public Torneo Torneo {get; set;}
        public int EquipoId {get; set;}
        public Equipo Equipo {get; set;}

    }
}