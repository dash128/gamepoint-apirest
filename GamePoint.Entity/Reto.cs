namespace GamePoint.Entity
{
    public class Reto
    {
        public int Id {get; set;}
        public int Ganador {get; set;}

        
        public int EquipoID1 {get; set;}
        public Equipo Equipo1 {get; set;}
        public int EquipoID2 {get; set;}
        public Equipo Equipo2 {get; set;}
    }
}