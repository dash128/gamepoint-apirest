using System;

namespace GamePoint.Entity
{
    public class Equipo
    {
        public int Id {get; set;}
        public string Nombre {get; set;}
        public DateTime FechaCreacion {get; set;}


        public int JuegoId {get; set;}
        public Juego Juego {get; set;}
    }
}