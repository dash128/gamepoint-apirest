namespace GamePoint.Entity
{
    public class Miembro
    {
        public int Id {get; set;}
        public bool Capitan {get; set;}


        public int EquipoId {get; set;}
        public Equipo Equipo {get;set;}
        public int JugadorId {get; set;}
        public Jugador Jugador {get; set;}
    }
}