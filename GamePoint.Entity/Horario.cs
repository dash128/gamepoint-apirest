using System;

namespace GamePoint.Entity
{
    public class Horario
    {
        public int Id {get; set;}
        public DateTime FechaDia {get; set;}
        public int HoraInicio {get; set;}
        public bool Disponibilidad {get; set;}


        public int LanCenterId {get; set;}
        // public LanCenter LanCenter {get; set;}
        public int CabinaId {get; set;}
        // public Cabina Cabina {get; set;}
    }
}