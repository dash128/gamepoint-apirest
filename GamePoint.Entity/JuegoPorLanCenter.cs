namespace GamePoint.Entity
{
    public class JuegoPorLanCenter
    {
        public int Id {get; set;}


        public int LanCenterId {get; set;}
        public LanCenter LanCenter {get; set;}
        public int JuegoId {get; set;}
        public Juego Juego {get; set;}
    }
}