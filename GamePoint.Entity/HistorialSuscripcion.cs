using System;

namespace GamePoint.Entity
{
    public class HistorialSuscripcion
    {
        public int Id {get; set;}
        public DateTime FechaInicio {get; set;}
        public DateTime FechaFin {get; set;}


        public int LanCenterId {get; set;}
        public LanCenter LanCenter {get; set;}
        public int SuscripcionId {get; set;}
        public Suscripcion Suscripcion {get; set;}
    }
}