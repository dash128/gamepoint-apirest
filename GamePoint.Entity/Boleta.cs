using System;

namespace GamePoint.Entity
{
    public class Boleta
    {
        public int Id {get; set;}
        public DateTime FechaMenbresia {get; set;}
        public double Pago {get; set;}


        public int LanCenterId {get; set;}
        public LanCenter LanCenter {get; set;}
        public int SuscripcionId {get; set;}
        public Suscripcion Suscripcion {get; set;}
    }
}