using System;

namespace GamePoint.Entity
{
    public class Reserva
    {
        public int Id {get; set;}
        public DateTime FechaReserva {get; set;}
        public string Estado {get; set;}


        public int JugadorId {get; set;}
        public Jugador Jugador {get; set;}
        public int HorarioId {get; set;}
        public Horario Horario {get; set;}
        
    }
}