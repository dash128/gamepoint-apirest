using System;

namespace GamePoint.Entity
{
    public class Torneo
    {
        public int Id {get; set;}
        public string Nombre {get; set;}
        public DateTime FechaComienzo {get; set;}


        public int LanCenterId {get; set;}
        public LanCenter LanCenter {get; set;}
        public int JuegoId {get; set;}
        public Juego Juego {get; set;}
    }
}