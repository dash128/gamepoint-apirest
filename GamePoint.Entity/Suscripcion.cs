namespace GamePoint.Entity
{
    public class Suscripcion
    {
        public int Id {get; set;}
        public string Nombre {get; set;}
        public string Duracion {get; set;}
        public decimal Precio {get; set;}
    }
}