using System;

namespace GamePoint.Entity
{
    public class Jugador
    {
        public int Id {get; set;}
        public string Nombre {get; set;}
        public string Celular {get; set;}
        public string Correo {get; set;}
        public string Contrasena {get; set;}
        public DateTime FechaNacimiento {get; set;}
    }
}