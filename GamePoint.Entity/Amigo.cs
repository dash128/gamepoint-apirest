namespace GamePoint.Entity
{
    public class Amigo
    {
        public int Id {get; set;}


        public int JugadorID1 {get; set;}
        public Jugador Jugador1 {get; set;}
        public int JugadorID2 {get; set;}
        public Jugador Jugador2 {get; set;}
    }
}