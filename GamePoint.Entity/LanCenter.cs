namespace GamePoint.Entity
{
    public class LanCenter
    {
        public int Id {get; set;}
        public string Nombre {get; set;}
        public string Celular {get; set;}
        public string Correo {get; set;}
        public string Contrasena {get; set;}
        public string Distrito {get; set;}

        public float Latitud {get; set;}
        public float Longitud {get; set;}
    }
}